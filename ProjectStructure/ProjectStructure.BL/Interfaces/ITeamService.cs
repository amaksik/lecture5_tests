﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Common.Models.DTO;

namespace ProjectStructure.BL.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAllTeams();

        void CreateTeam(TeamDTO team);
        void UpdateTeam(TeamDTO team);
        void DeleteTeam(int teamId);
    }
}
